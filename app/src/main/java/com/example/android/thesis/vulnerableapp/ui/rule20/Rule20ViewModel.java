package com.example.android.thesis.vulnerableapp.ui.rule20;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class Rule20ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Rule20ViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is Rule20 fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}