package com.example.android.thesis.vulnerableapp.ui.rule21;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.android.thesis.vulnerableapp.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Rule21Fragment extends Fragment {

    int SERVER_PORT = 5000;
    String SERVER_IP = "127.0.0.1";
    Thread Thread1 = null;
    EditText etIP, etPort;
    TextView tvMessages;
    EditText etMessage;
    Button btnSend;
    Button btnConnect;
    private Rule21ViewModel rule21ViewModel;
    private PrintWriter output;
    private BufferedReader input;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rule21ViewModel =
                ViewModelProviders.of(this).get(Rule21ViewModel.class);
        final View root = inflater.inflate(R.layout.fragment_rule21, container, false);

        try {
            etIP = root.findViewById(R.id.etIP);
            etPort = root.findViewById(R.id.etPort);
            tvMessages = root.findViewById(R.id.tvMessages);
            etMessage = root.findViewById(R.id.etMessage);
            btnSend = root.findViewById(R.id.btnSend);
            btnConnect = root.findViewById(R.id.btnConnect);

            btnConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    tvMessages.setText("");
                    SERVER_IP = etIP.getText().toString().trim();
                    SERVER_PORT = Integer.parseInt(etPort.getText().toString().trim());
                    Thread1 = new Thread(new Thread1());
                    Thread1.start();
                }
            });
            btnSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String message = etMessage.getText().toString().trim();
                    if (!message.isEmpty()) {
                        new Thread(new Thread3(message)).start();
                    }
                }
            });
        } catch (Exception e) {
            Log.e("çççç", e.getMessage());
        }
        return root;
    }

    class Thread1 implements Runnable {
        public void run() {
            Socket socket;
            try {
                socket = new Socket(SERVER_IP, SERVER_PORT);
                output = new PrintWriter(socket.getOutputStream());
                input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tvMessages.setText("Connected\n");
                    }
                });
                new Thread(new Thread2()).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    class Thread2 implements Runnable {
        @Override
        public void run() {
            while (true) {
                try {
                    final String message = input.readLine();
                    if (message != null) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                tvMessages.append("server: " + message + "\n");
                            }
                        });
                    } else {
                        Thread1 = new Thread(new Thread1());
                        Thread1.start();
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    class Thread3 implements Runnable {
        private final String message;

        Thread3(String message) {
            this.message = message;
        }

        @Override
        public void run() {
            output.write(message + "\n");
            output.flush();
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    tvMessages.append("client: " + message + "\n");
                    etMessage.setText("");
                }
            });
        }
    }
}