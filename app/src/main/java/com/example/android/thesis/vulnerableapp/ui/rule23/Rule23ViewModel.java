package com.example.android.thesis.vulnerableapp.ui.rule23;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class Rule23ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Rule23ViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is Rule23 fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}