package com.example.android.thesis.vulnerableapp.ui.rule30;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

public class Rule30ViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public Rule30ViewModel() {
        mText = new MutableLiveData<>();
//        mText.setValue("This is Rule30 fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}