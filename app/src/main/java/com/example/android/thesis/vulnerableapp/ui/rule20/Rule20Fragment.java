package com.example.android.thesis.vulnerableapp.ui.rule20;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;

import com.example.android.thesis.vulnerableapp.R;

public class Rule20Fragment extends Fragment {

    private Rule20ViewModel rule20ViewModel;

    private void sendIntent() {
        String uri = "https://www.facebook.com";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        startActivity(intent);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        rule20ViewModel =
                ViewModelProviders.of(this).get(Rule20ViewModel.class);
        View root = inflater.inflate(R.layout.fragment_rule20, container, false);

        Button sendIntentButton = (Button) root.findViewById(R.id.button_rule20);
        sendIntentButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        sendIntent();
                    }
                }
        );

        return root;
    }
}
